<?php  
$a = true && false;  
var_dump($a); 
echo "<br />"; 
$b = false && true;  
var_dump($b); 
echo "<br />"; 
$c = true && true;  
var_dump($c); 
echo "<br />"; 
$d = false && false;  
var_dump($d); 
echo "<br />"; 
$a = true || false;  
var_dump($a); 
echo "<br />"; 
$b = false || true;  
var_dump($b); 
echo "<br />"; 
$c = true || true;  
var_dump($c); 
echo "<br />"; 
$d = false || false;  
var_dump($d);  
?>  